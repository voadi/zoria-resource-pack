# Zoria Resource Pack for Solarus

> :warning: This project is a work-in-progress and is not ready to be used.
> We'll remove this message when that changes.

Everything you need to start an 8-bit adventure game is here!
When used with [Solarus](https://www.solarus-games.org/), you can create many different types of environments and characters.

The pixel art is 4 colors per tile, and tiles are 16x16 pixels.
It uses the Zoria color palette, a modified NES color palette:

![Zoria color palette](https://gitlab.com/voadi/voadi/wikis/assets/zoria-palette.png)  
[Zoria.gpl](https://gitlab.com/voadi/voadi/wikis/assets/Zoria.gpl)

## Usage

1. [Install Solarus.](https://www.solarus-games.org/en/solarus/download)
2. [Download the resource pack.](https://gitlab.com/voadi/zoria-resource-pack/-/archive/master/zoria-resource-pack-master.zip)
3. Extract the ZIP somewhere on your computer.
4. Launch the Solarus Quest Editor.

### For new quests:

5. Choose "File > Load quest..."
6. Navigate to the downloaded resource pack and choose it.
7. Build your quest!

### For existing quests:

5. Choose "File > Import from a quest..."
6. Navigate to the downloaded resource pack and choose it.
7. Choose the assets you want to import and click "Import" on each of them.

## About

[Zoria Tileset](https://opengameart.org/content/zoria-tileset) is a CC-BY tileset created by [DragonDePlatino](https://twitter.com/DragonDePlatino).
It was used by VOADI which inspired the creation of more compatible assets.
This resource pack contains not just assets used in VOADI, but also many free assets contributed by others.

The resource pack is maintained by @basxto, who has converted many freely available assets from [OpenGameArt.org](https://opengameart.org/) to conform with Zoria restrictions.

## License

All assets are licensed under an approved free culture license, such as CC-BY, CC-BY-SA, GPL, FDL, and the like.

To see the license and attribution for a specific asset, please view it through the Quest Editor (all asset metadata is visible in the Solarus GUI).
